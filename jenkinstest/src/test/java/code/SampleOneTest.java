package code;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class SampleOneTest {

	@Test
	public void printHello() throws IOException {
		
		System.out.println("_____________________________________");
		System.out.println("* Welcome to Jenkins. Congratuations! * ");
	    System.out.println("_____________________________________");
	
	   // chrome test

		
	   ChromeOptions options = new ChromeOptions();
	   options.addArguments("headless");
       options.addArguments("window-size=1200x600");   
	  WebDriver driver = new ChromeDriver(options);
		
//WebDriver driver = new ChromeDriver();
       driver.get("http://seleniumhq.org");
       // a guarantee that the test was really executed
       assertTrue("Passed!",driver.findElement(By.id("q")).isDisplayed());
       System.out.println("******************");
      System.out.println("**Test is done!**");
       System.out.println("******************");
       driver.quit();

	   
	}

	

}
